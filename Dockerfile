FROM node:20.8-bookworm-slim

WORKDIR /app

COPY package.json package-lock.json ./
COPY package-lock.json ./

RUN npm install
# unmodified files

COPY . .


# RUN npm run build

EXPOSE 3000
CMD ["node", "app.js"]


